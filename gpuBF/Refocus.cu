/**
 @file gpuBF/Refocus.cu
 @author Dongwoon Hyun (dongwoon.hyun@stanford.edu)
 @date 2022-05-05

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "Refocus.cuh"

namespace rtbf {

template <typename T_in, typename T_out>
Refocus<T_in, T_out>::Refocus(std::shared_ptr<Tensor<T_in>> input,
                              const Tensor<float> *h_txDelays,
                              const Tensor<float> *h_txApods,
                              float samplingFrequency, cudaStream_t cudaStream,
                              std ::string moniker, std::string loggerName) {
  // Load inputs with error checking
  loadInputs(input, cudaStream, moniker, loggerName);

  // Load transmit delays and apodizations with error checking
  txd = Tensor<float>(*h_txDelays, gpuID, this->label + "txdel", this->logName);
  txa = Tensor<float>(*h_txApods, gpuID, this->label + "txapo", this->logName);
  fs = samplingFrequency;
  auto ddims = txd.getDimensions();  // Get dimensions of transmit delays
  auto adims = txa.getDimensions();  // Get dimensions of transmit apodizations
  if (ddims.size() != 2 || adims.size() != 2)
    this->template logerror<std::invalid_argument>(
        "Transmit delays and apodizations must have shape [nelemT, nxmits].");
  if (ddims != adims)
    this->template logerror<std::invalid_argument>(
        "Transmit delays and apods must have the same shape. Found {} and {}.",
        vecString(ddims), vecString(adims));
  if (ddims[1] != nxmits)
    this->template logerror<std::invalid_argument>(
        "Input dimension mismatch. Data contains {} transmits, but delay and "
        "apodization contain {} transmits.",
        nxmits, ddims[1]);

  // The first dimension of the transmit delay and apodizations is nelemT
  nelemT = ddims[0];

  // Initialize cuFFT handles
  initHandles();
  // Obtain E, the transmit encoder
  makeEncoder();
  // Obtain D, the inverse matrices to decode the transmit delays.
  makeDecoder();
  // Initialize the tmp and this->out arrays
  initOutput();
}

template <typename T_in, typename T_out>
Refocus<T_in, T_out>::Refocus(
    std::shared_ptr<Tensor<T_in>> input,
    const Tensor<cuda::std::complex<float>> *h_encoder,
    const Tensor<cuda::std::complex<float>> *h_decoder, cudaStream_t cudaStream,
    std ::string moniker, std::string loggerName) {
  if (h_encoder == nullptr && h_decoder == nullptr) {
    this->template logerror<std::invalid_argument>(
        "No encoder or decoder supplied.");
  } else if (h_encoder != nullptr && h_decoder != nullptr) {
    this->logwarn(
        "Both encoder or decoder supplied. Ignoring the encoder and using the "
        "decoder only.");
  }
  // Load inputs with error checking
  loadInputs(input, cudaStream, moniker, loggerName);

  if (h_decoder == nullptr) {
    // If no decoder is supplied, use the encoder to create a decoder
    E = Tensor<cuda::std::complex<float>>(*h_encoder, gpuID,
                                          this->label + "->E", this->logName);
    auto encdims = E.getDimensions();
    if (encdims.size() != 3 || encdims[1] != nxmits || encdims[2] != nsamps)
      this->logerror(
          "Transmit decoder must be 3D: [nelemT, nxmits, nsamps]. Received {}.",
          vecString(encdims));
    nelemT = encdims[0];
    makeDecoder();
  } else {
    // Otherwise, use the supplied decoder
    D = Tensor<cuda::std::complex<float>>(*h_decoder, gpuID,
                                          this->label + "->D", this->logName);
    auto decdims = D.getDimensions();
    if (decdims.size() != 3 || decdims[1] != nxmits || decdims[2] != nsamps)
      this->logerror(
          "Transmit decoder must be 3D: [nelemT, nxmits, nsamps]. Received {}.",
          vecString(decdims));
    nelemT = decdims[0];
  }

  // Initialize cuFFT handles
  initHandles();
  // Initialize the tmp and this->out arrays
  initOutput();
}

template <typename T_in, typename T_out>
void Refocus<T_in, T_out>::loadInputs(std::shared_ptr<Tensor<T_in>> input,
                                      cudaStream_t cudaStream,
                                      std ::string moniker,
                                      std::string loggerName) {
  this->label = moniker;
  this->setLogger(loggerName);
  this->logdebug("Initializing {}.", this->label);
  // Throw compiler warning if attempting to use unsupported datatypes
  static_assert(std::is_same_v<T_in, cuda::std ::complex<float>> &&
                    std::is_same_v<T_in, T_out>,
                "Refocus currently requires complex float input and output.");

  // Read inputs
  this->in = input;                 // Share ownership of input Tensor
  this->stream = cudaStream;        // Set asynchronous stream
  gpuID = this->in->getDeviceID();  // Execute on input's device

  // Infer input data properties. It is expected that the data will be organized
  // as [number of RF samples, number of transmits, number of elements].
  auto idims = this->in->getDimensions();  // Get dimensions of input
  if (idims.size() < 3)
    this->logerror(
        "Input to Refocus must be at least 3D: [nsamps, nxmits, nelemR, ...]. "
        "Received {}.",
        vecString(idims));
  nsamps = idims[0];
  nxmits = idims[1];
  nelemR = idims[2];

  // Count number of frames
  nframe = 1;
  for (int i = 3; i < idims.size(); i++) nframe *= idims[i];

  CCE(cublasCreate(&handle));                  // Initialize cublas handle
  CCE(cublasSetStream(handle, this->stream));  // Initialize cublas stream
}

template <typename T_in, typename T_out>
void Refocus<T_in, T_out>::initHandles() {
  CCE(cufftCreate(&fftplan1));
  CCE(cufftCreate(&fftplan2));
  int p = nsamps;            // Number of rows of data pitch
  int y1 = nxmits * nelemT;  // Number of columns of valid data
  int y2 = nelemT * nelemR;  // Number of channels in data
  CCE(cufftPlanMany(&fftplan1, 1, &p, &p, 1, p, &p, 1, p, CUFFT_C2C, y1));
  CCE(cufftPlanMany(&fftplan2, 1, &p, &p, 1, p, &p, 1, p, CUFFT_C2C, y2));
  CCE(cufftSetStream(fftplan1, this->stream));
  CCE(cufftSetStream(fftplan2, this->stream));
}

template <typename T_in, typename T_out>
void Refocus<T_in, T_out>::initOutput() {
  // Define data dimensions
  std::vector<size_t> tdims = {nsamps, nelemR, std::max(nelemT, nxmits)};
  std::vector<size_t> odims = {nsamps, nelemT, nelemR};

  // Initialize output array and other private arrays
  tmp = Tensor<cuda::std::complex<float>>(tdims, gpuID, this->label + "->tmp",
                                          this->logName);
  this->out = std::make_shared<Tensor<T_out>>(
      odims, gpuID, this->label + "->out", this->logName);
}

template <typename T_in, typename T_out>
Refocus<T_in, T_out>::~Refocus() {
  this->loginfo("Destroyed {}.", this->label);
}

template <typename T_in, typename T_out>
void Refocus<T_in, T_out>::makeEncoder() {
  // Construct the encoder matrix according to the transmit delays and
  // apodizations. This approach assumes that the transmit sequence can be
  // described as a simple weighted time delay, i.e. E = txa * exp(2j*pi*f*txd)
  // To specify more complex transmit sequences (e.g., multi-line transmits),
  // the user should supply the encoder matrix directly.
  std::vector<size_t> edims{nelemT, nxmits, nsamps};
  E = Tensor<cuda::std::complex<float>>(edims, gpuID, this->label + "->E",
                                        this->logName);

  // Create the encoder tensor that represents the actual transmit sequence
  // For each frequency, the matrix E has size [nelemT, nxmits]
  dim3 blk(16, 4, 4);
  dim3 grd((nelemT - 1) / blk.x + 1, (nxmits - 1) / blk.y + 1,
           (nsamps - 1) / blk.z + 1);
  kernels::Refocus::makeEncoder<<<grd, blk, 0, this->stream>>>(
      E.data(), txd.data(), txa.data(), fs, nelemT, nxmits, nsamps);
}

template <typename T_in, typename T_out>
void Refocus<T_in, T_out>::makeDecoder() {
  // Denote the encoder matrix as E and its Hermitian transpose as Eh.
  // Given E, the decoder is computed as E / (Eh * E + λI), where "/" means the
  // quantity on the right is inverted, and λ is the amount of diagonal loading,
  // selected heuristically. This code is not optimized and is not intended for
  // use in the real-time loop. It should be called once at graph instantiation.

  // To make the decoder, we will require several intermediate Tensors.
  std::vector<size_t> ehedims{nxmits, nxmits, nsamps};
  std::vector<size_t> invdims{nelemT, nxmits, nsamps};
  auto EhE = Tensor<cuda::std::complex<float>>(
      ehedims, gpuID, this->label + "->E'*E", this->logName);
  auto invEhE = Tensor<cuda::std::complex<float>>(
      ehedims, gpuID, this->label + "->inv(E'*E)", this->logName);
  D = Tensor<cuda::std::complex<float>>(invdims, gpuID, this->label + "->D",
                                        this->logName);
  tikhonov_EhE(EhE);             // EhE = Eh * E + λI;
  tikhonov_invEhE(invEhE, EhE);  // invEhE = inv(Eh * E + λI)
  tikhonov_EinvEhE(invEhE);      // EinvEhE = E * inv(Eh * E + λI)
}

template <typename T_in, typename T_out>
void Refocus<T_in, T_out>::tikhonov_EhE(
    Tensor<cuda::std::complex<float>> &EhE) {
  // Compute (Eh * E + λI) with size [nxmits, nxmits] (Tikhonov
  // regularization) First, apply diagonal loading to make inversion better
  // posed
  dim3 blk(16, 4, 4);
  dim3 grd((nxmits - 1) / blk.x + 1, (nxmits - 1) / blk.y + 1,
           (nsamps - 1) / blk.z + 1);
  float lambda = nelemT * nelemT / 100;  // Heuristic
  kernels::Refocus::loadDiagonal<<<grd, blk, 0, this->stream>>>(
      EhE.data(), lambda, nxmits, nsamps);
  // Then, call cuBLAS for matrix multiplication
  // Matrix multiply using batched and strided Cgemm3m
  // A: Eh  (nxmits, nelemT, nfreqs)
  // B: E   (nelemT, nxmits, nfreqs)
  // C: EhE (nxmits, nxmits, nfreqs)
  // Compute C = alpha*A*B + beta*C on a batched/strided basis
  int m = nxmits;
  int n = nxmits;
  int k = nelemT;
  const cuComplex alpha = make_float2(1.f, 0.f);
  auto *A = reinterpret_cast<cuComplex *>(E.data());
  int lda = nelemT;
  long long int strideA = nelemT * nxmits;
  auto *C = reinterpret_cast<cuComplex *>(EhE.data());
  int ldc = nxmits;
  long long int strideC = nxmits * nxmits;
  CCE(cublasCgemm3mStridedBatched(handle, CUBLAS_OP_C, CUBLAS_OP_N, m, n, k,
                                  &alpha, A, lda, strideA, A, lda, strideA,
                                  &alpha, C, ldc, strideC, nsamps));
}

template <typename T_in, typename T_out>
void Refocus<T_in, T_out>::tikhonov_invEhE(
    Tensor<cuda::std::complex<float>> &invEhE,
    Tensor<cuda::std::complex<float>> &EhE) {
  // Perform in-place LU decomposition, P*A=L*U
  cuComplex **h_A = (cuComplex **)malloc(sizeof(cuComplex *) * nsamps);
  for (int i = 0; i < nsamps; i++)
    h_A[i] = reinterpret_cast<cuComplex *>(EhE.data() + i * nxmits * nxmits);
  cuComplex **Aarray;
  CCE(cudaMalloc((void **)&Aarray, sizeof(cuComplex *) * nsamps));
  CCE(cudaMemcpyAsync(Aarray, h_A, sizeof(cuComplex *) * nsamps,
                      cudaMemcpyDefault, this->stream));
  int n = nxmits;
  int lda = nxmits;
  int *pivot, *info;
  CCE(cudaMalloc((void **)&pivot, sizeof(int) * n * nsamps));
  CCE(cudaMalloc((void **)&info, sizeof(int) * nsamps));
  CCE(cublasCgetrfBatched(handle, n, Aarray, lda, pivot, info, nsamps));

  // Perform out-of-place inversion to get D(E'* E + λ*I)
  cuComplex **h_C = (cuComplex **)malloc(sizeof(cuComplex *) * nsamps);
  for (int i = 0; i < nsamps; i++)
    h_C[i] = reinterpret_cast<cuComplex *>(invEhE.data() + i * nxmits * nxmits);
  cuComplex **Carray;
  CCE(cudaMalloc((void **)&Carray, sizeof(cuComplex *) * nsamps));
  CCE(cudaMemcpyAsync(Carray, h_C, sizeof(cuComplex *) * nsamps,
                      cudaMemcpyDefault, this->stream));
  int ldc = nxmits;
  CCE(cublasCgetriBatched(handle, n, Aarray, lda, pivot, Carray, ldc, info,
                          nsamps));

  CCE(cudaFree(Carray));
  free(h_C);
  CCE(cudaFree(info));
  CCE(cudaFree(pivot));
  CCE(cudaFree(Aarray));
  free(h_A);
}

template <typename T_in, typename T_out>
void Refocus<T_in, T_out>::tikhonov_EinvEhE(
    Tensor<cuda::std::complex<float>> &invEhE) {
  // Finally, perform the inversion as E * D(E'*E+λ*I)
  // Matrix multiply using batched and strided Cgemm3m
  // A: E      (nelemT, nxmits, nfreqs)
  // B: invEhE (nxmits, nxmits, nfreqs)
  // C: D    (nelemT, nxmits, nfreqs)
  // Compute C = alpha*A*B + beta*C on a batched/strided basis
  int m = nelemT;
  int n = nxmits;
  int k = nxmits;
  const cuComplex alpha = make_float2(1.f, 0.f);
  auto *A = reinterpret_cast<cuComplex *>(E.data());
  int lda = nelemT;
  long long int strideA = nelemT * nxmits;
  auto *B = reinterpret_cast<cuComplex *>(invEhE.data());
  int ldb = nxmits;
  long long int strideB = nxmits * nxmits;
  const cuComplex beta = make_float2(0.f, 0.f);
  auto *C = reinterpret_cast<cuComplex *>(D.data());
  int ldc = nelemT;
  long long int strideC = nelemT * nxmits;
  CCE(cublasCgemm3mStridedBatched(handle, CUBLAS_OP_N, CUBLAS_OP_N, m, n, k,
                                  &alpha, A, lda, strideA, B, ldb, strideB,
                                  &beta, C, ldc, strideC, nsamps));
  this->loginfo("Computed decoder.");
}

template <typename T_in, typename T_out>
void Refocus<T_in, T_out>::decode() {
  if (gpuID < 0)
    this->template logerror<NotImplemented>(
        "No CPU implementation of Refocus yet.");
  static_assert(std::is_same_v<T_in, cuda::std ::complex<float>> &&
                    std::is_same_v<T_in, T_out>,
                "Refocus currently requires complex float input and output.");
  // Set CUDA device
  CCE(cudaSetDevice(gpuID));
  cufftComplex one{1.f}, zero{0.f};
  int nfreqs = nsamps;

  // Get the first frame pointer for each
  auto *rptr0 = reinterpret_cast<cufftComplex *>(this->in->data());
  auto *tptr0 = reinterpret_cast<cufftComplex *>(tmp.data());
  auto *optr0 = reinterpret_cast<cufftComplex *>(this->out->data());

  for (int frame = 0; frame < nframe; frame++) {
    // Get the current frame's pointer
    auto *rptr = rptr0 + frame * (nsamps * nxmits * nelemR);
    auto *tptr = tptr0;
    auto *optr = optr0 + frame * (nsamps * nelemT * nelemR);

    // Apply forward FFT to the raw data in the sample dimension
    CCE(cufftExecC2C(fftplan1, rptr, tptr, CUFFT_FORWARD));

    // Transpose the raw data to prepare for matrix multiplication
    // (nsamps, nxmits, nelemR) --> (nxmits, nelemR, nsamps)
    dim3 B1(16, 16);
    dim3 G1((nxmits * nelemR - 1) / B1.x + 1, (nsamps - 1) / B1.y + 1);
    kernels::Refocus::transpose<<<G1, B1, 0, this->stream>>>(
        tptr, rptr, nxmits * nelemR, nfreqs);
    tmp.resetToZerosAsync(this->stream);

    // Matrix multiply using batched and strided Cgemm3m
    // A: D (nelemT, nxmits, nfreqs)
    // B: raw (nxmits, nelemR, nfreqs)
    // C: out (nelemT, nelemR, nfreqs)
    // Compute C = alpha*A*B + beta*C on a batched/strided basis
    auto *A = reinterpret_cast<cufftComplex *>(D.data());
    int lda = nelemT;
    int strideA = D.getPitch() * nxmits;
    auto *B = rptr;
    int ldb = nxmits;
    int strideB = ldb * nelemR;
    auto *C = tptr;
    int ldc = nelemT;
    int strideC = ldc * nelemR;
    CCE(cublasCgemm3mStridedBatched(
        handle, CUBLAS_OP_N, CUBLAS_OP_N, nelemT, nelemR, nxmits, &one, A, lda,
        strideA, B, ldb, strideB, &zero, C, ldc, strideC, nfreqs));

    // Transpose the multistatic dataset back to (nfreqs, nelemT, nelemR)
    dim3 B2(16, 16);
    dim3 G2((nfreqs - 1) / B2.x + 1, (nelemT * nelemR - 1) / B2.y + 1);
    kernels::Refocus::transpose<<<G2, B2, 0, this->stream>>>(tptr, optr, nfreqs,
                                                             nelemT * nelemR);

    // Undo the FFT to get back our multistatic data array
    CCE(cufftExecC2C(fftplan2, optr, optr, CUFFT_INVERSE));
  }
}

///////////////////////////////////////////////////////////////////////////
// kernels
///////////////////////////////////////////////////////////////////////////

template <typename T_in, typename T_out>
__global__ void kernels::Refocus::transpose(T_in *src, T_out *dst, int nx_dst,
                                            int ny_dst) {
  // Determine information about the current thread
  int x = blockIdx.x * blockDim.x + threadIdx.x;
  int y = blockIdx.y * blockDim.y + threadIdx.y;
  if (x < nx_dst && y < ny_dst) dst[x + nx_dst * y] = src[y + ny_dst * x];
}

__global__ void kernels::Refocus::makeEncoder(cuda::std::complex<float> *E,
                                              float *txdel, float *txapo,
                                              float fs, int nelemT, int nxmits,
                                              int nsamps) {
  // Determine information about the current thread
  int x = blockIdx.x * blockDim.x + threadIdx.x;
  int y = blockIdx.y * blockDim.y + threadIdx.y;
  int z = blockIdx.z * blockDim.z + threadIdx.z;
  if (x < nelemT && y < nxmits && z < nsamps) {
    int eidx = x + nelemT * (y + nxmits * z);
    int tidx = x + nelemT * y;
    float txd = txdel[tidx];    // Delays
    float txa = txapo[tidx];    // Apodization
    float f = fs * z / nsamps;  // Frequency
    E[eidx] = expj2pi(f * txd) * txa;
  }
}

__global__ void kernels::Refocus::loadDiagonal(cuda::std::complex<float> *ete,
                                               float lambda, int nxmits,
                                               int nsamps) {
  // Determine information about the current thread
  int x = blockIdx.x * blockDim.x + threadIdx.x;
  int y = blockIdx.y * blockDim.y + threadIdx.y;
  int z = blockIdx.z * blockDim.z + threadIdx.z;
  if (x < nxmits && y < nxmits && z < nsamps) {
    int idx = x + nxmits * (y + nxmits * z);
    ete[idx] = x == y ? lambda : 0.f;
  }
}
// TODO: Add support for other input, output data types.
template class Refocus<cuda::std::complex<float>, cuda::std::complex<float>>;
}  // namespace rtbf