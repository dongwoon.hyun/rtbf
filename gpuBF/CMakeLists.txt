project(gpuBF LANGUAGES CUDA CXX)

set(SOURCES
  Tensor.cu
  Bmode.cu
  ChannelSum.cu
  Decimate.cu
  Demodulate.cu
  Focus.cu
  FocusLUT.cu
  Hilbert.cu
  InputLoader.cu
  Refocus.cu
  SVDFilter.cu
  ScanConvert2D.cu
)

add_library(gpuBF STATIC ${SOURCES})

target_link_libraries(gpuBF cufft cublas cusolver spdlog::spdlog)

if(RTBF_BUILD_GPUBF_TESTS)
  add_subdirectory(test)
endif()
