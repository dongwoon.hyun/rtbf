/**
 @file gpuBF/ScanConvert2D.cuh
 @author Dongwoon Hyun (dongwoon.hyun@stanford.edu)
 @date 2022-08-18

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef SCANCONVERT2D_CUH_
#define SCANCONVERT2D_CUH_

#include "Operator.cuh"

namespace rtbf {

enum class ScanConvert2DMode { SECTOR, CURVILINEAR, PHASED };

/** @brief Class to apply a spatiotemporal clutter filter to data.
 */
template <typename T_in, typename T_out>
class ScanConvert2D : public Operator<Tensor<T_in>, Tensor<T_out>> {
 protected:
  // CUDA objects
  int gpuID;
  Tensor<float> r;           ///< Depths corresponding to data
  Tensor<float> a;           ///< Angles corresponding to data
  float apex;                ///< Apex offsets for each data line (> 0)
  cudaTextureObject_t tex;   ///< Texture object
  cudaTextureDesc texDesc;   ///< Texture description
  cudaResourceDesc resDesc;  ///< Resource description for texture
  Tensor<float> pxpos;       ///< Desired pixel positions
  Tensor<float> ri;          ///< Depths to interpolate at
  Tensor<float> ai;          ///< Angles to interpolate at
  size_t npixels;            ///< Number of pixels to interpolate at
  size_t nframes;            ///< Number of frames
  ScanConvert2DMode mode;    ///< Scan conversion mode
  std::shared_ptr<Tensor<T_in>> in_pitched;  ///< Make a pitched copy if needed

  /// @brief Initializer for the texture object.
  void initTextureObject();

 public:
  /// @brief Constructor
  ScanConvert2D(std::shared_ptr<Tensor<T_in>> input,
                const Tensor<float> *h_depths, const Tensor<float> *h_angles,
                const Tensor<float> *pixelPositions,
                ScanConvert2DMode mode = ScanConvert2DMode::SECTOR,
                float apexDist = 0.f, cudaStream_t cudaStream = 0,
                std ::string moniker = "ScanConvert2D",
                std::string loggerName = "");
  virtual ~ScanConvert2D();

  /// @brief Apply scan conversion
  void convert();
};

namespace kernels::ScanConvert2D {
/// @cond KERNELS
/**	@addtogroup ScanConvert2D Kernels
        @{
*/
/** @brief Compute the look-up-tables for scan conversion
 @param r Pointer to r values (depths) of polar-coordinate data
 @param nr Number of r values (depths) of polar-coordinate data
 @param a Pointer to azimuth angles of polar-coordinate data
 @param na Number of azimuth angles of polar-coordinate data
 @param pxpos Cartesian coordinates of desired pixels
 @param ri Look-up table for each pixel to the correct r value index
 @param ai Look-up table for each pixel to the correct a value index
 @param npx Number of pixels in the scan-converted (Cartesian) image
 @param mode Scan conversion mode {'SECTOR', 'CURVILINEAR', 'PHASED'}
 @param apex The z-offset of the polar origin vs. Cartesian origin
*/
__global__ void computeLUT(float *r, int nr, float *a, int na, float3 *pxpos,
                           float *ri, float *ai, int npx,
                           ScanConvert2DMode mode, float apex);

/** @brief Apply scan conversion based on pre-computed LUTs
 @param tex Input data texture object
 @param npx Number of pixels in the scan-converted (Cartesian) image
 @param ri Look-up table for each pixel to the correct r value index
 @param ai Look-up table for each pixel to the correct a value index
 @param out Pointer to output scan-converted image
*/
template <typename T_out>
__global__ void applyConversion(cudaTextureObject_t tex, int npx, float *ri,
                                float *ai, T_out *out);

/** @}*/
/// @endcond

}  // namespace kernels::ScanConvert2D

// Add template type traits for SFINAE
template <typename>
struct is_ScanConvert2D : std::false_type {};
template <typename T_in, typename T_out>
struct is_ScanConvert2D<ScanConvert2D<T_in, T_out>> : std::true_type {};

}  // namespace rtbf

#endif /* SCANCONVERT_CUH_ */
