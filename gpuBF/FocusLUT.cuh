/**
 @file gpuBF/FocusLUT.cuh
 @author Dongwoon Hyun (dongwoon.hyun@stanford.edu)
 @date 2022-04-05

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef RTBF_FOCUSLUT_CUH_
#define RTBF_FOCUSLUT_CUH_

#include "Operator.cuh"
#include "interp1d.cuh"

namespace rtbf {

/** @brief Class to apply focusing delays to data using delay and apodization
look-up tables.

Unlike Focus, FocusLUT uses look-up tables of delays and apodizations to apply
beamforming. This provides flexibility when the
*/
template <typename T_in, typename T_out>
class FocusLUT : public Operator<Tensor<T_in>, Tensor<T_out>> {
 protected:
  // CUDA objects
  std::vector<cudaTextureObject_t> tex;  ///< Texture object
  cudaTextureDesc texDesc;               ///< Texture description
  cudaResourceDesc resDesc;              ///< Resource description for texture
  InterpMode imode;  ///< Interpolation mode (e.g., LINEAR or CUBIC)
  BFMode tmode;      ///< Beamforming mode
  BFMode rmode;      ///< Beamforming mode
  // Device version of variables
  std::unique_ptr<Tensor<float>> txdel;  ///< Focusing delays (TX)
  std::unique_ptr<Tensor<float>> txapo;  ///< Focusing apodization (TX)
  std::unique_ptr<Tensor<float>> rxdel;  ///< Focusing delays (RX)
  std::unique_ptr<Tensor<float>> rxapo;  ///< Focusing apodization (RX)
  float fs;      ///< Sampling frequency that data was acquired at
  float fc;      ///< Center frequency of original RF signal
  float c0;      ///< Speed of sound
  float dz;      ///< Pixel spacing in z
  int gpuID;     ///< Device for execution
  bool isBBIn;   ///< Boolean flag for whether input is baseband
  bool isBBOut;  ///< Boolean flag for whether output should be baseband
  std::vector<size_t> pdims;  ///< Size of pixel dimensions
  size_t nxmits;              ///< Number of transmit events
  size_t nelems;              ///< Number of receive elements

  // Compile-time type checking (both T_in and T_out must be real or complex)
  static_assert(
      is_complex<T_in>() == is_complex<T_out>(),
      "FocusOTF performs real->real and complex->complex beamforming, but does "
      "not perform real->complex or complex->real beamforming.");

  /// @brief Initializer for the texture object.
  void initTextureObject();
  /// @brief Templatized kernel dispatcher.
  template <InterpMode imode>
  void focusHelper();
  /// @brief Input dimension checker. TODO: Need to restore functionality.
  void checkAllInputs();
  /// @brief Prepare the output Tensor based on beamforming modes.
  void makeOutputTensor();

 public:
  /** @brief FocusLUT constructor.
   @param input Shared pointer to an input Tensor.
   @param txDelay Per-pixel delays to add to each transmit event [s].
   @param rxDelay Per-pixel delays to add to each receive element [s].
   @param txApod Per-pixel apodizations to add to each transmit event.
   @param rxApod Per-pixel apodizations to add to each receive element.
   @param fsample Sampling frequency [Hz].
   @param fcenter Center frequency [Hz].
   @param soundspeed Assumed reconstruction sound speed [m/s].
   @param isInputBB Whether input is modulated or baseband.
   @param makeOutputBB Whether to produce modulated or baseband output.
   @param pixelZSpacing Spacing between subsequent depth samples in output [m].
   @param txbfMode Transmit beamforming mode (SUM, IDENTITY, or BLKDIAG).
   @param rxbfMode Receive beamforming mode (SUM or IDENTITY).
   @param intrpMode Interpolation mode (NEAREST, LINEAR, CUBIC, or LANCZOS3).
   @param cudaStream CUDA stream for execution.
   @param moniker Name for FocusLUT Operator.
   @param loggerName The name of the spdlog logger to use.
  */
  FocusLUT(std::shared_ptr<Tensor<T_in>> input, const Tensor<float> *txDelay,
           const Tensor<float> *rxDelay, const Tensor<float> *txApod,
           const Tensor<float> *rxApod, float fsample, float fcenter,
           float soundspeed, bool isInputBB, bool makeOutputBB,
           float pixelZSpacing, BFMode txbfMode = BFMode::BLKDIAG,
           BFMode rxbfMode = BFMode::IDENTITY,
           InterpMode intrpMode = InterpMode::CUBIC,
           cudaStream_t cudaStream = 0, std ::string moniker = "FocusLUT",
           std::string loggerName = "");
  /// @brief FocusLUT destructor.
  virtual ~FocusLUT();

  /// @brief Apply focusing and apodization to data
  void focus();
};

namespace kernels::FocusLUT {

/** @brief Core focusing kernel.
 @param tex Texture object of the raw data.
 @param tdel Device pointer to transmit delays (nullptr if none).
 @param rdel Device pointer to receive delays (nullptr if none).
 @param tapo Device pointer to transmit apodizations (nullptr if none).
 @param rapo Device pointer to receive apodizations (nullptr if none).
 @param npx Total number of pixels in output image
 @param ntxo Number of transmits to output (e.g., 1 if SUM, else nxmits).
 @param nrxo Number of receives to output (e.g., 1 if SUM, else nelems).
 @param fs Sampling frequency.
 @param fc Center frequency.
 @param nrows Size of the innermost pixel dimension.
 @param dz Physical spacing in depth between output samples.
 @param c0inv Inverse of sound speed. (Inverse to avoid costly divisions.)
 @param ntxsum Number of transmits to sum together (nxmits if SUM, else 1).
 @param nrxsum Number of receives to sum together (nelems if SUM, else 1).
 @param out Device pointer to output data.
 @param bb_input Whether input data is baseband or modulated.
 @param bb_output Whether output data should be baseband or modulated.
*/
template <typename T_out, InterpMode imode>
__global__ void focus_tables(cudaTextureObject_t tex, float *tdel, float *rdel,
                             float *tapo, float *rapo, int npx, int ntxo,
                             int nrxo, float fs, float fc, int nrows, float dz,
                             float c0inv, int ntxsum, int nrxsum, T_out *out,
                             bool bb_input, bool bb_output);

}  // namespace kernels::FocusLUT

// Add template type traits for SFINAE
template <typename>
struct is_FocusLUT : std::false_type {};
template <typename T_in, typename T_out>
struct is_FocusLUT<FocusLUT<T_in, T_out>> : std::true_type {};
}  // namespace rtbf

#endif /* RTBF_FOCUSLUT_CUH_ */
