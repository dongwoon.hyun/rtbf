/**
 @file gpuBF/Hilbert.cuh
 @author Dongwoon Hyun (dhyun)
 @date 2022-04-21

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef RTBF_HILBERT_CUH_
#define RTBF_HILBERT_CUH_

#include "Operator.cuh"
#include "cufft.h"
#include "gpuBF.cuh"

namespace rtbf {
/** @brief Class to apply in-place Hilbert Transform

This class takes a real signal (stored as an interleaved complex array with
zeros for the imaginary component) and computes the Hilbert Transform using
the cuFFT API. The transform is accomplished via a forward FFT, eliminating
the negative frequencies and doubling the positive frequencies, followed by a
final inverse FFT.
*/
template <typename T_in, typename T_out>
class Hilbert : public Operator<Tensor<T_in>, Tensor<T_out>> {
 private:
  std::vector<size_t> dims;  ///< Dimensions of transform
  int gpuID;                 ///< Execution device
  cufftHandle fftplan;       ///< CUFFT plan object
  std::shared_ptr<Tensor<cuda::std::complex<float>>>
      tmp;  ///< Temp. Tensor (if T_in != cuda::std::complex<float>)

 public:
  Hilbert(std::shared_ptr<Tensor<T_in>> input, cudaStream_t cudaStream = 0,
          std::string moniker = "Hilbert", std::string loggerName = "");
  virtual ~Hilbert();

  /// @brief Hilbert transform of raw RF data
  void transform();
};

namespace kernels::Hilbert {
__global__ void bpfilter(cuda::std::complex<float> *data, int pitch, int nsamps,
                         int nlines);
template <typename T>
__global__ void castToComplexFloat(cuda::std::complex<float> *dst, int dpitch,
                                   T *src, int spitch, int nsamps, int nlines);
template <typename T>
__global__ void castFromComplexFloat(T *dst, int dpitch,
                                     cuda::std::complex<float> *src, int spitch,
                                     int nsamps, int nlines);
}  // namespace kernels::Hilbert

// Add template-based type traits
template <typename>
struct is_Hilbert : std::false_type {};
template <typename T_in, typename T_out>
struct is_Hilbert<Hilbert<T_in, T_out>> : std::true_type {};

}  // namespace rtbf

#endif /* RTBF_HILBERT_CUH_ */
