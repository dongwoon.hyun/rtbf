/**
 @file test/test_Focus.cu
 @author Dongwoon Hyun (dongwoon.hyun@stanford.edu)
 @date 2019-08-17
*/

#include "Focus.cuh"
#include "gtest/gtest.h"

namespace rtbf {
namespace FocusTest {
namespace cstd = cuda::std;

using MyTypes = ::testing::Types<float, cstd::complex<float>>;

// Use a test fixture to reuse resource between tests
template <typename T>
class FocusTest : public ::testing::Test {
 protected:
  // Set up the test suite
  FocusTest() {}
  // Tear down the test suite
  ~FocusTest() {}
};

TYPED_TEST_SUITE(FocusTest, MyTypes);

TYPED_TEST(FocusTest, RunPlaneWave) {
  std::vector<size_t> pdims = {3, 480, 360};
  size_t npixels = prod(pdims) / 3;
  size_t nsamps = 2048;
  size_t nxmits = 25;  // x.size();
  size_t nelems = 128;
  auto P = Tensor<float>(pdims, -1, "h_pxpos");
  for (int j = 0; j < pdims[1]; j++) {
    for (int i = 0; i < pdims[0]; i++) {
      size_t idx = i + pdims[0] * j;
      P[idx + npixels * 0] = (1.f * j - (pdims[1] - 1) / 2) * 0.001;
      P[idx + npixels * 1] = 0.f;
      P[idx + npixels * 2] = (1.f * i - (pdims[0] - 1) / 2) * 0.001;
      // spdlog::debug("Pixel {:2d}: ({}, {}, {})", idx, x[j], y[k], z[i]);
    }
  }

  std::vector<size_t> edims = {3, nelems};
  auto E = Tensor<float>(edims, -1, "h_elpos");
  for (int i = 0; i < nelems; i++) {
    E[i + nelems * 0] = (1.f * i - (nelems - 1) / 2) * 0.0002;
    E[i + nelems * 1] = 0;
    E[i + nelems * 2] = 0;
  }

  std::vector<size_t> idims = {nsamps, nxmits, nelems};
  auto I = std::make_shared<Tensor<TypeParam>>(idims, 0, "iq");

  std::vector<size_t> tdims = {2, nxmits};
  auto T = Tensor<float>(tdims, -1, "h_txdir");
  std::vector<size_t> zdims = {nxmits};
  auto Z = Tensor<float>(zdims, -1, "h_time0");

  Focus<TypeParam, TypeParam> F{
      I,    &P,   &E, &T, &Z,     80000000.f,  8000000.f,
      true, true, 2,  2,  1540.f, BFMode::SUM, BFMode::SUM};
  F.focus();
}

}  // namespace FocusTest
}  // namespace rtbf
