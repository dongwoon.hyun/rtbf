/**
 @file test/test_Tensor.cu
 @author Dongwoon Hyun (dongwoon.hyun@stanford.edu)
 @date 2019-08-17
*/

#include "Tensor.cuh"
#include "gtest/gtest.h"

namespace rtbf {
namespace TensorTest {

TEST(TensorTest, InitGPU) {
  std::vector<size_t> d = {100, 10, 1};
  Tensor<float> A(d);
}
TEST(TensorTest, InitCPU) {
  std::vector<size_t> d = {100, 10, 1};
  Tensor<float> A(d, -1);
}
TEST(TensorTest, InitOneDim) {
  std::vector<size_t> d = {7};
  Tensor<float> A(d);
}
TEST(TensorTest, InitManyDim) {
  std::vector<size_t> d = {7, 3, 4, 3, 4, 5, 1, 2};
  Tensor<float> A(d);
}

TEST(TensorTest, Memcpy) {
  // Create a Tensor
  std::vector<size_t> d = {7, 11, 9, 3};
  Tensor<float> A(d);
  // Make dummy data
  srand(time(NULL));
  size_t size = prod(d);
  std::vector<float> h_truth(size);
  std::vector<float> h_array(size);
  for (int i = 0; i < size; i++) {
    h_truth[i] = (float)(rand() % 16) * 1.7f;
  }
  // Copy memory to GPU and back
  A.copyFrom(h_truth.data());
  A.copyTo(h_array.data());
  // Test for accuracy
  float sqerr = 0.f;
  for (int i = 0; i < size; i++) {
    float err = (h_truth[i] - h_array[i]);
    sqerr += err * err;
  }
  ASSERT_EQ(sqerr, 0.f);
}

TEST(TensorTest, MemcpyAsync) {
  // Create a Tensor
  cudaStream_t stream;
  CCE(cudaStreamCreate(&stream));
  std::vector<size_t> d = {7, 11, 9, 3};
  Tensor<float> A(d);
  // Make dummy data
  srand(time(NULL));
  size_t size = prod(d);
  std::vector<float> h_truth(size);
  std::vector<float> h_array(size);
  for (int i = 0; i < size; i++) {
    h_truth[i] = (float)(rand() % 16) * 1.7f;
  }
  // Copy memory to GPU and back
  A.copyFromAsync(h_truth.data(), stream);
  A.copyToAsync(h_array.data(), stream);
  // Test for accuracy
  float sqerr = 0.f;
  for (int i = 0; i < size; i++) {
    float err = (h_truth[i] - h_array[i]);
    sqerr += err * err;
  }
  ASSERT_EQ(sqerr, 0.f);
  CCE(cudaStreamDestroy(stream));
}

TEST(TensorTest, TensorViewCopy) {
  // Select dimensions
  std::vector<size_t> d = {7, 11, 9, 3};
  // Make dummy data
  srand(time(NULL));
  size_t size = prod(d);
  std::vector<float> h_truth(size);
  std::vector<float> h_array(size);
  for (int i = 0; i < size; i++) {
    h_truth[i] = (float)(rand() % 16) * 1.7f;
  }
  // Construct a Tensor
  Tensor<float> A(d, 0);
  TensorView<float> B(d, A.data());
  // Copy memory to device and back
  A.copyFrom(h_truth.data());
  B.copyTo(h_array.data());
  // Test for accuracy
  float sqerr = 0.f;
  for (int i = 0; i < size; i++) {
    float err = (h_truth[i] - h_array[i]);
    sqerr += err * err;
  }
  ASSERT_EQ(sqerr, 0.f);
}

TEST(TensorTest, ComplexFloat) {
  typedef std::complex<float> cfloat;
  // Create a Tensor
  std::vector<size_t> d = {7, 11, 9, 3};
  Tensor<cfloat> A(d);
  // Make dummy data
  srand(time(NULL));
  size_t size = prod(d);
  std::vector<cfloat> h_truth(size);
  std::vector<cfloat> h_array(size);
  for (int i = 0; i < size; i++) {
    h_truth[i].real((float)(rand() % 16) * 1.7f);
    h_truth[i].imag((float)(rand() % 16) * 9.2f);
  }
  // Copy memory to GPU and back
  A.copyFrom(h_truth.data());
  A.copyTo(h_array.data());
  // Test for accuracy
  float sqerr = 0.f;
  for (int i = 0; i < size; i++) {
    sqerr += std::norm(h_truth[i] - h_array[i]);
  }
  ASSERT_EQ(sqerr, 0.f);
}

TEST(TensorTest, ComplexShort) {
  typedef std::complex<short> cshort;
  // Create a Tensor
  std::vector<size_t> d = {7, 11, 9, 3};
  Tensor<cshort> A(d);
  // Make dummy data
  srand(time(NULL));
  size_t size = prod(d);
  std::vector<cshort> h_truth(size);
  std::vector<cshort> h_array(size);
  for (int i = 0; i < size; i++) {
    h_truth[i].real((short)(rand() % 16 * 1.7f));
    h_truth[i].imag((short)(rand() % 16 * 9.2f));
  }
  // Copy memory to GPU and back
  A.copyFrom(h_truth.data());
  A.copyTo(h_array.data());
  // Test for accuracy
  float sqerr = 0.f;
  for (int i = 0; i < size; i++) {
    sqerr += std::norm(h_truth[i] - h_array[i]);
  }
  ASSERT_EQ(sqerr, 0.f);
}

TEST(TensorTest, Reshape) {
  // Create a Tensor
  std::vector<size_t> d = {7, 11, 9, 3};
  Tensor<float> A(d);
  std::vector<size_t> dd = {7, 99, 3};
  A.reshape(dd);
  for (int i = 0; i < dd.size(); i++) ASSERT_EQ(A.getDimensions()[i], dd[i]);
}

TEST(TensorTest, CopyTensor) {
  // Test gratuitous copying to and from same device
  std::vector<size_t> d = {7, 11, 9, 3};
  auto size = prod(d);
  Tensor<float> A(d, -1);  // Initialize Tensor on device -1 (CPU)
  for (int i = 0; i < size; i++) {
    A[i] = (float)(rand() % 16) * 1.7f;
  }
  auto B = Tensor<float>(A, 0);   // Make a copy on device 0 (GPU)
  auto C = Tensor<float>(B, 0);   // Make a copy on device 0 (GPU)
  auto D = Tensor<float>(C, -1);  // Make a copy on device -1 (CPU)
  auto E = Tensor<float>(D, -1);  // Make a copy on device -1 (CPU)
  Tensor<float> F(d, -1);         // Initialize Tensor on device -1 (CPU)
  F.copyFrom(E.data());
  // Test for accuracy
  float sqerr = 0.f;
  for (int i = 0; i < size; i++) {
    float err = (A[i] - F[i]);
    sqerr += err * err;
  }
  ASSERT_EQ(sqerr, 0.f);
}

TEST(TensorTest, CopyTensorTest) {
  // Test gratuitous copying to and from same device
  std::vector<size_t> d = {7, 11, 9, 3};
  auto size = prod(d);
  Tensor<float> A(d, -1, "A");  // Initialize Tensor on device -1 (CPU)
  for (int i = 0; i < size; i++) {
    A[i] = (float)(rand() % 16) * 1.7f;
  }
  auto B = Tensor<float>(A, 0);   // Make a copy on device 0 (GPU)
  auto C = Tensor<float>(B, 0);   // Make a copy on device 0 (GPU)
  auto D = Tensor<float>(C, -1);  // Make a copy on device -1 (CPU)
  auto E = Tensor<float>(D, -1);  // Make a copy on device -1 (CPU)
  Tensor<float> F(d, -1, "F");    // Initialize Tensor on device -1 (CPU)
  F.copyFrom(E.data());
  // Test for accuracy
  float sqerr = 0.f;
  for (int i = 0; i < size; i++) {
    float err = (A[i] - F[i]);
    sqerr += err * err;
  }
  ASSERT_EQ(sqerr, 0.f);
}

}  // namespace TensorTest
}  // namespace rtbf