/**
 @file bmode/rtbf_mex.cu
 @author Dongwoon Hyun (dongwoon.hyun@stanford.edu)
 @date 2022-05-01

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "rtbf_mex.cuh"

void MexFunction::addTensor(GraphData &g, mdat::Array arr, std::string name,
                            bool verb) {
  // Specify what to do for each data type that we encounter using lambda
  // functions specified via the [](){} syntax.
  std::string n = name;
  bool v = verb;
  mdat::apply_visitor(
      arr,
      overloaded{
          [&](mdat::TypedArray<short> &&a) { addTensor<short>(g, a, n, v); },
          [&](mdat::TypedArray<float> &&a) { addTensor<float>(g, a, n, v); },
          [&](mdat::TypedArray<sshort> &&a) { addTensor<sshort>(g, a, n, v); },
          [&](mdat::TypedArray<sfloat> &&a) { addTensor<sfloat>(g, a, n, v); },
          [&](auto &&a) {
            printErr(
                "Tensor must have type short, float, complex<short>, or "
                "complex<float>.");
          }});
}

template <typename T>
void MexFunction::addTensor(GraphData &graph, mdat::TypedArray<T> arr,
                            std::string name, bool verb) {
  // Get a non-owning TensorView of MATLAB array
  if constexpr (std::is_same_v<T, sshort>) {
    auto arr_t = getTensor<cshort>(arr);
    auto ptr = std::make_shared<Tensor<cshort>>(arr_t, gpuID, name);
    addTensor(graph, ptr, name, verb);
  } else if (std::is_same_v<T, sfloat>) {
    auto arr_t = getTensor<cfloat>(arr);
    auto ptr = std::make_shared<Tensor<cfloat>>(arr_t, gpuID, name);
    addTensor(graph, ptr, name, verb);
  } else {
    auto arr_t = getTensor<T>(arr);
    auto ptr = std::make_shared<Tensor<T>>(arr_t, gpuID, name);
    addTensor(graph, ptr, name, verb);
  }
}

template <typename T>
void MexFunction::addTensor(GraphData &graph, std::shared_ptr<Tensor<T>> a,
                            std::string name, bool verb) {
  // Ensure a unique name for this graph
  if (name.empty() || graph.tns.count(name)) {
    int i = 0;
    if (name.empty()) name = "tensor";
    while (graph.tns.count(name + "_" + std::to_string(i))) i++;
    name = name + "_" + std::to_string(i);
  }
  if (verb)
    printMsg("Adding tensor \"{}\" of datatype {} with dimensions {}.", name,
             type_name<T>(), vecString(a->getDimensions()));
  graph.tns[name] = std::move(a);
}

void MexFunction::addInputLoaderToGraph(GraphData &graph,
                                        const mdat::Array &arr,
                                        std::vector<size_t> dims) {
  std::string name = "InputLoader";
  std::string out_name = "input";
  if (dims.empty()) dims = arr.getDimensions();
  cudaStream_t &s = graph.stream;
  auto cmd = [](auto &&p) {};
  mdat::apply_visitor(
      arr, overloaded{[=, &graph, &s](mdat::TypedArray<short> &&a) {
                        addOperator<InputLoader, short>(
                            graph, name, out_name, cmd,
                            std::shared_ptr<Tensor<short>>(), dims, gpuID, s);
                      },
                      [=, &graph, &s](mdat::TypedArray<float> &&a) {
                        addOperator<InputLoader, float>(
                            graph, name, out_name, cmd,
                            std::shared_ptr<Tensor<float>>(), dims, gpuID, s);
                      },
                      [=, &graph, &s](mdat::TypedArray<sshort> &&a) {
                        addOperator<InputLoader, cshort>(
                            graph, name, out_name, cmd,
                            std::shared_ptr<Tensor<cshort>>(), dims, gpuID, s);
                      },
                      [=, &graph, &s](mdat::TypedArray<sfloat> &&a) {
                        addOperator<InputLoader, cfloat>(
                            graph, name, out_name, cmd,
                            std::shared_ptr<Tensor<cfloat>>(), dims, gpuID, s);
                      },
                      [&](auto &&a) {
                        printErr(
                            "Input must have type INT16, SINGLE, "
                            "COMPLEX_INT16, or COMPLEX_SINGLE. Found type {}.",
                            type_name(a.getType()));
                      }});
}

void MexFunction::addOperatorHelper(GraphData &graph,
                                    const mdat::StructArray &S,
                                    std::string &name, TensorType &input,
                                    std::string &out_name) {
  // Get a unique name for the operator
  name = getOptString(S, "name", getReqString(S, "operator"));
  if (graph.ops.count(name)) {
    int i = 0;
    while (graph.ops.count(name + "_" + std::to_string(i))) i++;
    name = name + "_" + std::to_string(i);
  }
  // Get the input Tensor by name
  std::string in_name = getOptString(S, "inputTensor", graph.lastTensor);
  if (graph.tns.count(in_name) == 0)
    printErr("\"{}\": Requested input Tensor \"{}\" is not in graph \"{}\".",
             name, in_name, graph.name);
  input = graph.tns[in_name];
  // Get the output Tensor's desired name
  out_name = getOptString(S, "outputTensor");
  if (out_name.empty()) out_name = name + "->out";
}

void MexFunction::addVSXFormatterToGraph(GraphData &graph,
                                         const mdat::StructArray &S) {
  std::string name, output_name;
  TensorType input;
  addOperatorHelper(graph, S, name, input, output_name);
  cudaStream_t &s = graph.stream;
  // Load VSXFormatter arguments
  size_t nsamps = getReqScalar(S, "nsamps");
  size_t nxmits = getReqScalar(S, "nxmits");
  size_t nelems = getReqScalar(S, "nelems");
  size_t nchans = getReqScalar(S, "nchans");
  size_t nframes = getOptScalar(S, "nframes", 1);
  size_t npulses = getOptScalar(S, "npulses", 1);
  auto vsxMode = getVSXSampleMode(getReqString(S, "sampleMode"));
  auto chmap = getOptTensorWithDims<int>(S, "chanmap", {nchans, nxmits});
  std::optional<int> dsf = getOptScalar(S, "dsf", -123456);
  if (dsf == -123456) dsf = std::nullopt;
  // Get executable function
  auto cmd = [&](auto &&x) {
    // if constexpr to avoid compiler errors when Operator != VSXFormatter.
    if constexpr (is_VSXFormatter<std::decay_t<decltype(*x)>>::value)
      x->formatRawVSXData();
  };
  // Add a VSXFormatter operator
  std::visit(overloaded{[&](std::shared_ptr<Tensor<short>> &p) {
                          addOperator<VSXFormatter>(
                              graph, name, output_name, cmd, p, nsamps, nxmits,
                              nelems, nchans, vsxMode, dsf, npulses, nframes,
                              chmap, gpuID, s);
                        },
                        [&](auto &&p) {
                          printErr(
                              "Expected VSXFormatter's input to have type "
                              "short. Found type {}.",
                              type_name<std::decay_t<decltype(p->val)>>());
                        }},
             input);
  graph.focus_baseband = vsxMode != VSXSampleMode::HILBERT;
  if (vsxMode == VSXSampleMode::BS50BW) {
    graph.vsx_dsf = 8;
  } else if (vsxMode == VSXSampleMode::BS100BW) {
    graph.vsx_dsf = 4;
  } else if (vsxMode == VSXSampleMode::NS200BW) {
    graph.vsx_dsf = 2;
  } else {
    graph.vsx_dsf = 1;
  }
}

void MexFunction::addHilbertToGraph(GraphData &graph,
                                    const mdat::StructArray &S) {
  // Load Hilbert arguments
  std::string name, output_name;
  TensorType input;
  addOperatorHelper(graph, S, name, input, output_name);
  cudaStream_t &s = graph.stream;
  // Get executable function
  auto cmd = [](auto &&x) {
    // if constexpr to avoid compiler errors when Operator != Hilbert.
    if constexpr (is_Hilbert<std::decay_t<decltype(*x)>>::value) x->transform();
  };
  auto fn = [=, &graph, &s](auto &&p) {
    addOperator<Hilbert>(graph, name, output_name, cmd, p, s);
  };
  // Add a Hilbert node to ops using most recent Operator's output as input
  std::visit(overloaded{[&](std::shared_ptr<Tensor<short>> &p) { fn(p); },
                        [&](std::shared_ptr<Tensor<float>> &p) { fn(p); },
                        [&](auto &&p) {
                          printErr(
                              "Expected Hilbert's input to have type short or "
                              "float. Found type {}.",
                              type_name<std::decay_t<decltype(p->val)>>());
                        }},
             input);
}

void MexFunction::addDemodulateToGraph(GraphData &graph,
                                       const mdat::StructArray &S) {
  // Load Demodulate arguments
  std::string name, output_name;
  TensorType input;
  addOperatorHelper(graph, S, name, input, output_name);
  cudaStream_t &s = graph.stream;
  // Load Focus arguments
  auto t = getReqTensor<float>(S, "t");  // Time vectors
  float fd = getReqScalar(S, "fdemod");  // Demodulation frequency
  bool ip = getReqScalar(S, "inplace");  // Flag for in-place computation
  // Get executable function
  auto cmd = [](auto &&x) {
    // if constexpr to avoid compiler errors when Operator != Demodulate.
    if constexpr (is_Demodulate<std::decay_t<decltype(*x)>>::value)
      x->demodulate();
  };
  auto fn = [=, &graph, &s](auto &&p) {
    addOperator<Demodulate>(graph, name, output_name, cmd, p, &t, fd, ip, s);
  };
  // Add a Demodulate node to ops using most recent Operator's output as input
  std::visit(
      overloaded{[&](std::shared_ptr<Tensor<short>> &p) { fn(p); },
                 [&](std::shared_ptr<Tensor<float>> &p) { fn(p); },
                 [&](std::shared_ptr<Tensor<cshort>> &p) { fn(p); },
                 [&](std::shared_ptr<Tensor<cfloat>> &p) { fn(p); },
                 [&](auto &&p) {
                   printErr(
                       "Expected Demodulate's input to have type short, float, "
                       "complex<short>, or complex<float>. Found type {}.",
                       type_name<std::decay_t<decltype(p->val)>>());
                 }},
      input);
}

void MexFunction::addDecimateToGraph(GraphData &graph,
                                     const mdat::StructArray &S) {
  std::string name, output_name;
  TensorType input;
  addOperatorHelper(graph, S, name, input, output_name);
  cudaStream_t &s = graph.stream;
  int dsf = getReqScalar(S, "dsf");
  int axis = getOptScalar(S, "axis", 0);
  // Load ChannelSum arguments
  auto cmd = [](auto &&x) {
    // if constexpr to avoid compiler errors when Operator != Decimate
    if constexpr (is_Decimate<std::decay_t<decltype(*x)>>::value) x->decimate();
  };
  std::visit(
      overloaded{
          [&, name, output_name, cmd](std::shared_ptr<Tensor<short>> &p) {
            addOperator<Decimate, short>(graph, name, output_name, cmd, p, dsf,
                                         axis, s);
          },
          [&, name, output_name, cmd](std::shared_ptr<Tensor<float>> &p) {
            addOperator<Decimate, float>(graph, name, output_name, cmd, p, dsf,
                                         axis, s);
          },
          [&, name, output_name, cmd](std::shared_ptr<Tensor<cshort>> &p) {
            addOperator<Decimate, cshort>(graph, name, output_name, cmd, p, dsf,
                                          axis, s);
          },
          [&, name, output_name, cmd](std::shared_ptr<Tensor<cfloat>> &p) {
            addOperator<Decimate, cfloat>(graph, name, output_name, cmd, p, dsf,
                                          axis, s);
          },
          [&](auto &&p) {
            printErr(
                "Expected Decimate's input to have type short, float, "
                "complex<short>, or complex<float>. Found type {}.",
                type_name<std::decay_t<decltype(p->val)>>());
          }},
      input);
}
void MexFunction::addFocusToGraph(GraphData &graph,
                                  const mdat::StructArray &S) {
  std::string name, output_name;
  TensorType input;
  addOperatorHelper(graph, S, name, input, output_name);
  cudaStream_t &s = graph.stream;
  // Load Focus arguments
  auto dims = getDimensions(input);
  auto pxpos = getReqTensor<float>(S, "pxpos");
  auto time0 = getReqTensorWithDims<float>(S, "time0", {dims[1]});
  auto elpos = getReqTensor<float>(S, "elpos");
  auto txdat = getReqTensor<float>(S, "txdat");
  float fs = getReqScalar(S, "fs");
  float fc = getReqScalar(S, "fc");
  float c0 = getReqScalar(S, "c0");
  auto txmode = getOptString(S, "txbf_mode");
  auto rxmode = getOptString(S, "rxbf_mode");
  float txfn = getOptScalar(S, "txfnum", 2);
  float rxfn = getOptScalar(S, "rxfnum", 2);
  bool inputBB = graph.focus_baseband;
  bool outputBB = graph.focus_baseband;
  if (fc == 0.f) {
    fc = getReqScalar(S, "fdemod");
    inputBB = true;
  }
  auto tdims = txdat.getDimensions();
  if (tdims.size() != 2 || (tdims[0] != 2 && tdims[0] != 3 && tdims[0] != 4) ||
      tdims[1] != dims[1])
    printErr(
        "Focus: Invalid transmit description txdat. Should have size [2 or 3, "
        "{}] but has size {}.",
        dims[1], vecString(tdims));
  auto edims = elpos.getDimensions();
  if (edims.size() != 2 || (edims[0] != 3 && edims[0] != 4) ||
      edims[1] != dims[2])
    printErr(
        "Focus: Invalid element positions elpos. Should have size [3 or 4, "
        "{}] but has size {}.",
        dims[1], vecString(edims));

  BFMode txbfMode = BFMode::SUM;
  BFMode rxbfMode = BFMode::IDENTITY;
  if (txmode.compare("BLKDIAG") == 0)
    txbfMode = BFMode::BLKDIAG;
  else if (txmode.compare("SUM") == 0)
    txbfMode = BFMode::SUM;
  else if (txmode.compare("IDENTITY") == 0)
    txbfMode = BFMode::IDENTITY;
  else
    printErr(
        "Unrecognized transmit beamforming mode. Received '{}' but expected "
        "'IDENTITY', 'SUM', or 'BLKDIAG'.",
        txmode);

  if (rxmode.compare("BLKDIAG") == 0)
    rxbfMode = BFMode::BLKDIAG;
  else if (rxmode.compare("SUM") == 0)
    rxbfMode = BFMode::SUM;
  else if (rxmode.compare("IDENTITY") == 0)
    rxbfMode = BFMode::IDENTITY;
  else
    printErr(
        "Unrecognized receive beamforming mode. Received '{}' but expected "
        "'IDENTITY', 'SUM', or 'BLKDIAG'.",
        rxmode);

  InterpMode iMode = InterpMode::CUBIC;
  auto cmd = [](auto &&x) {
    // if constexpr to avoid compiler errors when Operator != FocusLUT.
    if constexpr (is_Focus<std::decay_t<decltype(*x)>>::value) x->focus();
  };
  // In C++20, we will be able to used templated lambda functions to fold the RF
  // and IQ cases into a single lambda function.
  auto fnRF = [=, &graph, &s](auto &&p) {
    addOperator<Focus, float>(graph, name, output_name, cmd, p, &pxpos, &elpos,
                              &txdat, &time0, fs, fc, false, false, txfn, rxfn,
                              c0, txbfMode, rxbfMode, iMode, s);
  };
  auto fnIQ = [=, &graph, &s](auto &&p) {
    addOperator<Focus, cfloat>(graph, name, output_name, cmd, p, &pxpos, &elpos,
                               &txdat, &time0, fs, fc, inputBB, outputBB, txfn,
                               rxfn, c0, txbfMode, rxbfMode, iMode, s);
  };
  std::visit(overloaded{[&](std::shared_ptr<Tensor<float>> &p) { fnRF(p); },
                        [&](std::shared_ptr<Tensor<cfloat>> &p) { fnIQ(p); },
                        [&](auto &&p) {
                          printErr(
                              "Expected Focus's input to have type float or "
                              "complex<float>. Found type {}.",
                              type_name<std::decay_t<decltype(p->val)>>());
                        }},
             input);
}
void MexFunction::addFocusLUTToGraph(GraphData &graph,
                                     const mdat::StructArray &S) {
  std::string name, output_name;
  TensorType input;
  addOperatorHelper(graph, S, name, input, output_name);
  cudaStream_t &s = graph.stream;
  // Load FocusLUT arguments
  Tensor<float> txdel = getReqTensor<float>(S, "txdel");
  Tensor<float> rxdel = getReqTensor<float>(S, "rxdel");
  // auto txapo = getOptTensor<float>(S, "txapo"); // TODO: Implement
  // auto rxapo = getOptTensor<float>(S, "rxapo"); // TODO: Implement
  float dz = getReqScalar(S, "dz");
  float fs = getReqScalar(S, "fs");  // / graph.vsx_dsf;
  float fc = getReqScalar(S, "fc");
  float c0 = getReqScalar(S, "c0");
  bool inputBB = graph.focus_baseband;
  bool outputBB = graph.focus_baseband;
  if (fc == 0.f) {
    fc = getReqScalar(S, "fdemod");
    inputBB = true;
  }
  rtbf::BFMode txbfMode = rtbf::BFMode::SUM;       // TODO: Remove hard code
  rtbf::BFMode rxbfMode = rtbf::BFMode::IDENTITY;  // TODO: Remove hard code
  InterpMode iMode = InterpMode::CUBIC;
  auto cmd = [](auto &&x) {
    // if constexpr to avoid compiler errors when Operator != Focus.
    if constexpr (is_FocusLUT<std::decay_t<decltype(*x)>>::value) x->focus();
  };
  // In C++20, we will be able to used templated lambda functions to fold the
  // RF and IQ cases into a single lambda function.
  auto fnRF = [=, &graph, &s](auto &&p) {
    addOperator<FocusLUT, float>(graph, name, output_name, cmd, p, &txdel,
                                 &rxdel, nullptr, nullptr, fs, fc, c0, false,
                                 false, dz, txbfMode, rxbfMode, iMode, s);
  };
  auto fnIQ = [=, &graph, &s](auto &&p) {
    addOperator<FocusLUT, cfloat>(graph, name, output_name, cmd, p, &txdel,
                                  &rxdel, nullptr, nullptr, fs, fc, c0, inputBB,
                                  outputBB, dz, txbfMode, rxbfMode, iMode, s);
  };
  std::visit(overloaded{[&](std::shared_ptr<Tensor<float>> &p) { fnRF(p); },
                        [&](std::shared_ptr<Tensor<cfloat>> &p) { fnIQ(p); },
                        [&](auto &&p) {
                          printErr(
                              "Expected FocusLUT's input to have type float or "
                              "complex<float>. Found type {}.",
                              type_name<std::decay_t<decltype(p->val)>>());
                        }},
             input);
}
void MexFunction::addChannelSumToGraph(GraphData &graph,
                                       const mdat::StructArray &S) {
  std::string name, output_name;
  TensorType input;
  addOperatorHelper(graph, S, name, input, output_name);
  cudaStream_t &s = graph.stream;
  int axis = getOptScalar(S, "axis", -1);
  int nout = getOptScalar(S, "nchout", 1);
  // Load ChannelSum arguments
  auto cmd = [](auto &&x) {
    // if constexpr to avoid compiler errors when Operator != ChannelSum.
    if constexpr (is_ChannelSum<std::decay_t<decltype(*x)>>::value)
      x->sumChannels();
  };
  std::visit(
      overloaded{
          [&, name, output_name, cmd](std::shared_ptr<Tensor<short>> &p) {
            addOperator<ChannelSum, short>(graph, name, output_name, cmd, p,
                                           axis, nout, s);
          },
          [&, name, output_name, cmd](std::shared_ptr<Tensor<float>> &p) {
            addOperator<ChannelSum, float>(graph, name, output_name, cmd, p,
                                           axis, nout, s);
          },
          [&, name, output_name, cmd](std::shared_ptr<Tensor<cshort>> &p) {
            addOperator<ChannelSum, cshort>(graph, name, output_name, cmd, p,
                                            axis, nout, s);
          },
          [&, name, output_name, cmd](std::shared_ptr<Tensor<cfloat>> &p) {
            addOperator<ChannelSum, cfloat>(graph, name, output_name, cmd, p,
                                            axis, nout, s);
          },
          [&](auto &&p) {
            printErr(
                "Expected ChannelSum's input to have type short, float, "
                "complex<short>, or complex<float>. Found type {}.",
                type_name<std::decay_t<decltype(p->val)>>());
          }},
      input);
}
void MexFunction::addBmodeToGraph(GraphData &graph,
                                  const mdat::StructArray &S) {
  std::string name, output_name;
  TensorType input;
  addOperatorHelper(graph, S, name, input, output_name);
  cudaStream_t &s = graph.stream;
  // Load Bmode arguments
  auto mode = getOptString(S, "compression");
  float gamma = getOptScalar(S, "gamma", 0.3f);
  std::optional<int> axis = getOptScalar(S, "axis", -123456);
  if (axis == -123456) axis = std::nullopt;
  auto cmd = [mode, gamma](auto &&x) {
    // if constexpr to avoid compiler errors when Operator != Bmode.
    if constexpr (is_Bmode<std::decay_t<decltype(*x)>>::value) {
      if (mode.compare("log") == 0) {
        x->getEnvelopeLogCompress();
      } else if (mode.compare("power") == 0) {
        x->getEnvelopePowCompress(gamma);
      } else {
        x->getEnvelope();
      }
    }
  };
  auto fn = [=, &graph, &s](auto &&p) {
    addOperator<Bmode, float>(graph, name, output_name, cmd, p, axis, s);
  };
  std::visit(
      overloaded{[&](std::shared_ptr<Tensor<cshort>> &p) { fn(p); },
                 [&](std::shared_ptr<Tensor<cfloat>> &p) { fn(p); },
                 [&](auto &&p) {
                   printErr(
                       "Expected Bmode's input to have type complex<short> or "
                       "complex<float>. Found type {}.",
                       type_name<std::decay_t<decltype(p->val)>>());
                 }},
      input);
}

void MexFunction::addRefocusToGraph(GraphData &graph,
                                    const mdat::StructArray &S) {
  std::string name, output_name;
  TensorType input;
  addOperatorHelper(graph, S, name, input, output_name);
  cudaStream_t &s = graph.stream;
  // Load Refocus arguments
  auto dims = getDimensions(input);
  if (dims.size() < 2)
    printErr(
        "Expected the input Tensor to Refocus to have dimensions [# samples, # "
        "transmits, # rx elements]. Found dimensions {}.",
        vecString(dims));

  // Refocus will execute the following command when invoked.
  auto cmd = [](auto &&x) {
    // if constexpr to avoid compiler errors when Operator != Refocus.
    if constexpr (is_Refocus<std::decay_t<decltype(*x)>>::value) x->decode();
  };

  // There are three ways to initialize Refocus. In order of priority:
  //   1. Provide the decoder matrices
  //   2. Provide the encoder matrices (rtbf computes decoder)
  //   3. Provide the tx delay and apods (rtbf computes encoder and decoder)
  // If the decoder is present, it will use that. Otherwise, if the encoder is
  // present, it will use that.

  // 1. If the decoder is provided (either as fieldname "txinv" or "decoder")
  std::string field = "";
  std::vector<std::string> valid{"txinv", "decoder"};
  for (auto &str : valid) {
    if (isField(S, str)) field = str;
  }
  if (!field.empty()) {
    auto txinv = getReqTensor<cfloat>(S, field);
    auto idims = txinv.getDimensions();
    if (idims.size() != 3 || idims[1] != dims[1] || idims[2] != dims[0])
      printErr(
          "Expected the decoder Tensor for Refocus to have dimensions [# tx "
          "elements, #transmits, #samples]. Found dimensions {}.",
          vecString(idims));
    auto fn = [=, &graph, &s](auto &&p) {
      addOperator<Refocus>(graph, name, output_name, cmd, p, nullptr, &txinv,
                           s);
    };
    std::visit(overloaded{[&](std::shared_ptr<Tensor<cfloat>> &p) { fn(p); },
                          [&](auto &&p) {
                            printErr(
                                "Expected Refocus's input to have type "
                                "complex<float>. Found type {}.",
                                type_name<std::decay_t<decltype(p->val)>>());
                          }},
               input);
    return;
  }

  // 2. If the encoder is provided (either as fieldname "txenc" or "encoder")
  valid = std::vector<std::string>({"txenc", "encoder"});
  for (auto &str : valid) {
    if (isField(S, str)) field = str;
  }
  if (!field.empty()) {
    auto txenc = getReqTensor<cfloat>(S, field);
    auto idims = txenc.getDimensions();
    if (idims.size() != 3 || idims[1] != dims[1] || idims[2] != dims[0])
      printErr(
          "Expected the encoder Tensor for Refocus to have dimensions [# tx"
          " elements, #transmits, #samples. Found dimensions {}.",
          vecString(idims));
    auto fn = [=, &graph, &s](auto &&p) {
      addOperator<Refocus>(graph, name, output_name, cmd, p, &txenc, nullptr,
                           s);
    };
    std::visit(overloaded{[&](std::shared_ptr<Tensor<cfloat>> &p) { fn(p); },
                          [&](auto &&p) {
                            printErr(
                                "Expected Refocus's input to have type "
                                "complex<float>. Found type {}.",
                                type_name<std::decay_t<decltype(p->val)>>());
                          }},
               input);
    return;
  }

  // 3. If neither the encoder nor decoder are provided
  if (isField(S, "txdel") && isField(S, "txapo") && isField(S, "fs")) {
    auto txdel = getReqTensor<float>(S, "txdel");
    auto txapo = getReqTensor<float>(S, "txapo");
    float fs = getReqScalar(S, "fs");
    auto ddims = txdel.getDimensions();
    auto adims = txapo.getDimensions();
    if (ddims.size() != 2 || adims.size() != 2 || ddims != adims)
      printErr(
          "Expected the transmit delays and apodizations to have dimensions"
          "[#tx elements, #transmits]. Found dimensions {} and {}.",
          vecString(ddims), vecString(adims));

    auto fn = [=, &graph, &s](auto &&p) {
      addOperator<Refocus>(graph, name, output_name, cmd, p, &txdel, &txapo, fs,
                           s);
    };
    std::visit(overloaded{[&](std::shared_ptr<Tensor<cfloat>> &p) { fn(p); },
                          [&](auto &&p) {
                            printErr(
                                "Expected Refocus's input to have type "
                                "complex<float>. Found type {}.",
                                type_name<std::decay_t<decltype(p->val)>>());
                          }},
               input);
    return;
  }

  // If none of the options were satisfied, print help message.
  printErr(
      "The Refocus operator must be specified using one of three "
      "methods:\n\t1. a field named 'decoder'\n\t2. a field named 'encoder' "
      "(decoder computed automatically)\n\t3. fields named 'txdel', 'txapo', "
      "and 'fs' (encoder and decoder computed automatically.");
}

void MexFunction::addScanConvert2DToGraph(GraphData &graph,
                                          const mdat::StructArray &S) {
  std::string name, output_name;
  TensorType input;
  addOperatorHelper(graph, S, name, input, output_name);
  cudaStream_t &s = graph.stream;
  // Load ScanConvert2D arguments
  auto dims = getDimensions(input);
  auto depths = getReqTensor<float>(S, "r");
  auto angles = getReqTensor<float>(S, "theta");
  auto pxpos = getReqTensor<float>(S, "pxpos");
  float apex = getOptScalar(S, "apex", 0);

  ScanConvert2DMode mode = ScanConvert2DMode::SECTOR;  // Default
  if (apex != 0.f) {
    std::string modestr = getReqString(S, "mode");
    if (modestr.compare("SECTOR") == 0) {
      mode = ScanConvert2DMode::SECTOR;
    } else if (modestr.compare("CURVILINEAR") == 0) {
      mode = ScanConvert2DMode::CURVILINEAR;
    } else if (modestr.compare("PHASED") == 0) {
      mode = ScanConvert2DMode::PHASED;
    } else {
      printErr(
          "When specifying a non-zero apex, the ScanConvert2D operator mode "
          "must be 'SECTOR', 'CURVILINEAR', or 'PHASED'.");
    }
  }

  // ScanConvert2D will execute the following command when invoked.
  auto cmd = [](auto &&x) {
    // if constexpr to avoid compiler errors when Operator != ScanConvert2D.
    if constexpr (is_ScanConvert2D<std::decay_t<decltype(*x)>>::value)
      x->convert();
  };

  // Create separate lambda functions for each T_out
  auto fn1 = [=, &graph, &s](auto &&p) {
    addOperator<ScanConvert2D, float>(graph, name, output_name, cmd, p, &depths,
                                      &angles, &pxpos, mode, apex, s);
  };
  auto fn2 = [=, &graph, &s](auto &&p) {
    addOperator<ScanConvert2D, cfloat>(graph, name, output_name, cmd, p,
                                       &depths, &angles, &pxpos, mode, apex, s);
  };
  std::visit(overloaded{[&](std::shared_ptr<Tensor<float>> &p) { fn1(p); },
                        [&](std::shared_ptr<Tensor<cfloat>> &p) { fn2(p); },
                        [&](auto &&p) {
                          printErr(
                              "Expected ScanConvert2D's input to have type "
                              "float or complex<float>. Found type {}.",
                              type_name<std::decay_t<decltype(p->val)>>());
                        }},
             input);
}

void MexFunction::addSVDFilterToGraph(GraphData &graph,
                                      const mdat::StructArray &S) {
  std::string name, output_name;
  TensorType input;
  addOperatorHelper(graph, S, name, input, output_name);
  cudaStream_t &s = graph.stream;
  // Load SVDFilter arguments
  auto dims = getDimensions(input);
  // SVDFilter will execute the following command when invoked.
  auto cmd = [](auto &&x) {
    // if constexpr to avoid compiler errors when Operator != SVDFilter.
    if constexpr (is_SVDFilter<std::decay_t<decltype(*x)>>::value) x->filter();
  };

  auto weights = getReqTensor<float>(S, "weights");
  bool inplace = getOptScalar(S, "inplace", false);

  auto fn1 = [=, &graph, &s](auto &&p) {
    addOperator<SVDFilter, float>(graph, name, output_name, cmd, p, &weights,
                                  inplace, s);
  };
  auto fn2 = [=, &graph, &s](auto &&p) {
    addOperator<SVDFilter, cfloat>(graph, name, output_name, cmd, p, &weights,
                                   inplace, s);
  };
  std::visit(overloaded{[&](std::shared_ptr<Tensor<float>> &p) { fn1(p); },
                        [&](std::shared_ptr<Tensor<cfloat>> &p) { fn2(p); },
                        [&](auto &&p) {
                          printErr(
                              "Expected SVDFilter's input to have type float "
                              "or complex<float>. Found type {}.",
                              type_name<std::decay_t<decltype(p->val)>>());
                        }},
             input);
}

template <typename Tc>
void MexFunction::loadTensor(Tensor<Tc> *t, mdat::Array a,
                             const cudaStream_t &s) {
  mdat::apply_visitor(
      a, overloaded{
             [&](mdat::TypedArray<short> &&m) { loadTensor<short>(t, m, s); },
             [&](mdat::TypedArray<float> &&m) { loadTensor<float>(t, m, s); },
             [&](mdat::TypedArray<sshort> &&m) { loadTensor<sshort>(t, m, s); },
             [&](mdat::TypedArray<sfloat> &&m) { loadTensor<sfloat>(t, m, s); },
             [&](auto &&a) {
               printErr(
                   "Tensor must have type short, float, complex<short>, or "
                   "complex<float>.");
             }});
}
template <typename Tm, typename Tc>
void MexFunction::loadTensor(Tensor<Tc> *t, mdat::TypedArray<Tm> m,
                             const cudaStream_t &s) {
  std::vector<size_t> tdims = t->getDimensions();
  std::vector<size_t> mdims = m.getDimensions();
  bool isvalid = true;
  if (tdims.empty() || mdims.empty() || tdims[0] > mdims[0]) isvalid = false;
  int ntdims = 0, nmdims = 0;

  for (int i = 0; i < tdims.size(); i++) ntdims += tdims[i] > 1;
  for (int i = 0; i < mdims.size(); i++) nmdims += mdims[i] > 1;
  if (ntdims != nmdims) isvalid = false;
  for (int i = 1; i < ntdims; i++)
    if (tdims[i] > 1 && tdims[i] != mdims[i]) isvalid = false;
  if (!isvalid)
    printErr(
        "Cannot load a tensor of dimensions {} with a MATLAB array of "
        "dimensions {}.",
        vecString(tdims), vecString(mdims));
  // Get a non-owning TensorView of MATLAB array
  if constexpr (std::is_same_v<Tc, Tm>) {
    t->copyFromAsync(getDataPtr<Tc>(m), s, sizeof(Tc) * mdims[0]);
  } else if (std::is_same_v<Tc, cshort> && std::is_same_v<Tm, sshort>) {
    t->copyFromAsync(
        reinterpret_cast<Tc *>(const_cast<Tm *>(getDataPtr<Tm>(m))), s,
        sizeof(Tc) * mdims[0]);
  } else if (std::is_same_v<Tc, cfloat> && std::is_same_v<Tm, sfloat>) {
    t->copyFromAsync(
        reinterpret_cast<Tc *>(const_cast<Tm *>(getDataPtr<Tm>(m))), s,
        sizeof(Tc) * mdims[0]);
  } else {
    printErr(
        "The supplied MATLAB input of type {} with dimensions {} is "
        "incompatible with the graph's input tensor of type {} with "
        "dimensions {}.",
        type_name<Tm>(), vecString(mdims), type_name<Tc>(), vecString(tdims));
  }
}

void MexFunction::execute(GraphData &graph) {
  for (auto &&fn : graph.fns) {
    (fn)();
  }
}

VSXSampleMode MexFunction::getVSXSampleMode(std::string str) {
  if (str == "NS200BW")
    return VSXSampleMode::NS200BW;
  else if (str == "BS100BW")
    return VSXSampleMode::BS100BW;
  else if (str == "BS50BW")
    return VSXSampleMode::BS50BW;
  else
    return VSXSampleMode::HILBERT;
}
