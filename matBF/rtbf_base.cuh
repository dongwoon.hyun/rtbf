/**
 @file utils/rtbf_base.cuh
 @author Dongwoon Hyun (dongwoon.hyun@stanford.edu)
 @date 2022-04-29

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef RTBF_BASE_CUH_
#define RTBF_BASE_CUH_

#include <complex>
#include <cuda/std/complex>
#include <exception>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

// For some reason, CMake defines DLL_EXPORT_SYM, which gets redefined by
// mex.hpp. Undefine the variable to suppress downstream warnings.
#ifdef DLL_EXPORT_SYM
#undef DLL_EXPORT_SYM
#endif

#include "Operator.cuh"
#include "Tensor.cuh"
#include "cuda.h"
#include "gpuBF.cuh"
#include "mex.hpp"
#include "mexAdapter.hpp"
#include "spdlog/fmt/xchar.h"
#include "spdlog/sinks/rotating_file_sink.h"  // support for basic file logging
#include "spdlog/sinks/stdout_color_sinks.h"
#include "spdlog/spdlog.h"
#include "type_names_mat.h"

// Define namespace aliases to simplify the code
namespace rtbf {
static std::string logFile = "logs/log.txt";

// Helper type for visitor template magic
// From https://en.cppreference.com/w/cpp/utility/variant/visit
template <class... Ts>
struct overloaded : Ts... {
  using Ts::operator()...;
};
// explicit deduction guide (not needed as of C++20)
template <class... Ts>
overloaded(Ts...) -> overloaded<Ts...>;

/** @brief Base MEX function class with useful utility functions.
 */
class rtbf_base : public matlab::mex::Function {
 protected:
  std::shared_ptr<matlab::engine::MATLABEngine>
      matlabPtr;                         ///< Pointer to MATLAB engine
  matlab::data::ArrayFactory factory;    ///< Factory to create MATLAB arrays
  std::shared_ptr<spdlog::logger> slog;  ///< Shared pointer to spdlog logger.

  /// @brief Set the logger properties
  virtual void setLogger(std::string logFile = "logs/log.txt",
                         spdlog::level::level_enum level = spdlog::level::warn);

  /// @brief Utility for checking if file exists
  void checkFileExists(std::string fname);
  /// @brief Utility to check that a field exists in a struct
  void checkFieldExists(const matlab::data::StructArray &sarr,
                        std::string field, int sindex = 0);
  /// @brief Utility to check that a field exists in a struct
  void checkFieldsExist(const matlab::data::StructArray &sarr,
                        std::vector<std::string> fields, int sindex = 0);
  /// @brief Utility to return boolean whether a field exists in a struct
  bool isField(const matlab::data::StructArray &sarr, std::string field,
               int sindex = 0);
  /// @brief Ensure that the requested type T and MATLAB Array are the same type
  template <typename T>
  void checkTypeMatch(const matlab::data::Array &arr);

  /// @brief Get a required scalar from a struct array field with error checking
  double getReqScalar(const matlab::data::StructArray &sarr, std::string field,
                      int sindex = 0);
  /// @brief Get an optional scalar from a struct array field or return default
  double getOptScalar(const matlab::data::StructArray &sarr, std::string field,
                      double default_value = 0, int sindex = 0);
  /// @brief Utility to obtain the raw data pointer without invoking a deep copy
  template <typename T>
  const T *getDataPtr(const matlab::data::Array &arr);
  /// @brief Get a pointer to a required data array
  template <typename T>
  const T *getReqArray(const matlab::data::StructArray &sarr, std::string field,
                       int sindex = 0);
  /// @brief Get a pointer to an optional data array
  template <typename T>
  const T *getOptArray(const matlab::data::StructArray &sarr, std::string field,
                       int sindex = 0);
  /// @brief Get a pointer to a required data array with dimension checking
  template <typename T>
  const T *getReqArrayWithDims(const matlab::data::StructArray &sarr,
                               std::string field, std::vector<size_t> dims,
                               int sindex = 0);
  /// @brief Get a pointer to an optional data array with dimension checking
  template <typename T>
  const T *getOptArrayWithDims(const matlab::data::StructArray &sarr,
                               std::string field, std::vector<size_t> dims,
                               int sindex = 0);
  /// @brief Get a TensorView<T> from a MATLAB array
  template <typename T>
  TensorView<T> getTensor(const matlab::data::Array &arr,
                          std::string name = "");
  /// @brief Get a pointer to a required Tensor<T>
  template <typename T>
  TensorView<T> getReqTensor(const matlab::data::StructArray &sarr,
                             std::string field, int sindex = 0);
  /// @brief Get a pointer to an optional Tensor<T>
  template <typename T>
  std::optional<TensorView<T>> getOptTensor(
      const matlab::data::StructArray &sarr, std::string field, int sindex = 0);
  /// @brief Get a pointer to a required Tensor<T> with dimension checking
  template <typename T>
  TensorView<T> getReqTensorWithDims(const matlab::data::StructArray &sarr,
                                     std::string field,
                                     std::vector<size_t> dims, int sindex = 0);
  /// View @rief Get a pointer to an optionalTensor<T> with dimension checking
  template <typename T>
  std::optional<TensorView<T>> getOptTensorWithDims(
      const matlab::data::StructArray &sarr, std::string field,
      std::vector<size_t> dims, int sindex = 0);
  /// @brief Get a required string from a struct
  std::string getReqString(const matlab::data::StructArray &sarr,
                           std::string field, int sindex = 0);
  /// @brief Get an optional string from a struct, or return default string
  std::string getOptString(const matlab::data::StructArray &sarr,
                           std::string field, std::string defaultString = "",
                           int sindex = 0);
  /// @brief Find the first GPU that can use texture objects (CC >= 3.0)
  int getFirstAvailableGPU();
  /// @brief Convenience function to copy Tensor to MATLAB array synchronously
  template <typename T>
  auto copyToMATLABArray(const std::shared_ptr<Tensor<T>> &D);
  /// @brief Convenience function to copy Tensor to MATLAB array asynchronously
  template <typename T>
  auto copyToMATLABArrayAsync(const std::shared_ptr<Tensor<T>> &D,
                              cudaStream_t stream);
  /// @brief Convenience function to copy Operator to MATLAB array
  template <typename T1, typename T2>
  auto copyToMATLABArray(Operator<Tensor<T1>, Tensor<T2>> &Op);
  /// @brief Convenience function to copy Operator to MATLAB array
  template <typename T1, typename T2>
  auto copyToMATLABArrayAsync(Operator<Tensor<T1>, Tensor<T2>> &Op,
                              cudaStream_t stream);
  /// @brief Simple utility to print to MATLAB using the function `func`.
  void printMATLAB(std::string str, std::string func) {
    if (func == "disp") slog->info(str);
    if (func == "warning") slog->warn(str);
    if (func == "error") slog->error(str);
    matlabPtr->feval(
        func, 0, std::vector<matlab::data::Array>({factory.createScalar(str)}));
  }
  /// @brief Print a message to MATLAB using `disp`.
  void printMsg(std::string str) { printMATLAB(str, "disp"); }
  /// @brief Print a warning to MATLAB using `warning`.
  void printWrn(std::string str) { printMATLAB(str, "warning"); }
  /// @brief Print an error to MATLAB using `error` (also exits C++ code).
  void printErr(std::string str) { printMATLAB(str, "error"); }  // Also exits
  /// @brief Print a formatted message to MATLAB.
  template <typename FormatString, typename... Args>
  void printMsg(const FormatString &fmtstr, Args &&...args) {
    printMsg(fmt::format(fmtstr, std::forward<Args>(args)...));
  }
  /// @brief Print a formatted warning to MATLAB.
  template <typename FormatString, typename... Args>
  void printWrn(const FormatString &fmtstr, Args &&...args) {
    printWrn(fmt::format(fmtstr, std::forward<Args>(args)...));
  }
  /// @brief Print a formatted error to MATLAB.
  template <typename FormatString, typename... Args>
  void printErr(const FormatString &fmtstr, Args &&...args) {
    printErr(fmt::format(fmtstr, std::forward<Args>(args)...));
  }

 public:
  rtbf_base() {}
  virtual ~rtbf_base() {}
  virtual void operator()(matlab::mex::ArgumentList outputs,
                          matlab::mex::ArgumentList inputs) = 0;
};

/// Set an output log file for spdlog. Different levels of logging severity can
/// be set using the spdlog::set_level command (default spdlog::level::warn).
/// This function is defined as virtual, so the user is free to override this in
/// their eventual MexFunction that is derived from this class.
void rtbf_base::setLogger(std::string logFile,
                          spdlog::level::level_enum level) {
  // Create basic file logger (not rotated)
  slog = spdlog::rotating_logger_mt("logger", logFile, 5242880, 1);
  // auto sink = std::make_shared<spdlog::sinks::stdout_color_sink_mt>();
  // my_logger->sinks().push_back(sink);
  spdlog::set_default_logger(slog);
  spdlog::set_pattern("[%Y-%m-%d %T.%e][%=7!l]%v");
  spdlog::set_level(level);
}

/// Attempt to access the file `fname`, and throw a MATLAB error upon failure.
void rtbf_base::checkFileExists(std::string fname) {
  if (!std::ifstream(fname).good()) printErr("{} cannot be accessed.", fname);
}

/// Check whether the field `field` exists in the MATLAB struct array `sarr`, at
/// the array index `sindex`, and throw a MATLAB error if the field is not
/// found. This is like checking existence of `sarr[sindex].field` in MATLAB.
void rtbf_base::checkFieldExists(const matlab::data::StructArray &sarr,
                                 std::string field, int sindex) {
  try {
    auto d = sarr[sindex][field];
  } catch (matlab::data::InvalidFieldNameException) {
    throw matlab::data::InvalidFieldNameException(fmt::format(
        "Required field '{}' does not exist in this struct.", field));
  }
}

void rtbf_base::checkFieldsExist(const matlab::data::StructArray &sarr,
                                 std::vector<std::string> fields, int sindex) {
  for (auto &field : fields) checkFieldExists(sarr, field, sindex);
}

/// Like checkFieldExists, but returns a bool rather than throwing an error.
bool rtbf_base::isField(const matlab::data::StructArray &sarr,
                        std::string field, int sindex) {
  try {
    auto d = sarr[sindex][field];
    return true;
  } catch (matlab::data::InvalidFieldNameException) {
    return false;
  }
}

/// Attempt to load a *scalar* from a struct field (i.e. `sarr[sindex].field` in
/// MATLAB code), and throw a C++ `matlab::data::InvalidFieldNameException`
/// exception if it fails.
double rtbf_base::getReqScalar(const matlab::data::StructArray &sarr,
                               std::string field, int sindex) {
  try {
    matlab::data::Array d = sarr[sindex][field];
    if (d.getNumberOfElements() > 1)
      printWrn("Field {} was expected to be a scalar but is an array.", field);
    return static_cast<double>(d[0]);
  } catch (matlab::data::InvalidFieldNameException) {
    throw matlab::data::InvalidFieldNameException(fmt::format(
        "Required field '{}' does not exist in this struct.", field));
  }
}

/// Attempt to load a *scalar* from a struct field, and return the provided
/// default value if it fails.
double rtbf_base::getOptScalar(const matlab::data::StructArray &sarr,
                               std::string field, double default_value,
                               int sindex) {
  try {
    return getReqScalar(sarr, field, sindex);
  } catch (matlab::data::InvalidFieldNameException) {
    return default_value;
  }
}

/// Check whether the MATLAB array `arr` has the C++ data type `<T>`, and throw
/// a MATLAB error if there is a type mismatch.
template <typename T>
void rtbf_base::checkTypeMatch(const matlab::data::Array &arr) {
  auto dummy = factory.createArray<T>({0});
  if (dummy.getType() != arr.getType())
    printErr("Expected data of type {} but got data of type {}.",
             matlab_type_names[dummy.getType()],
             matlab_type_names[arr.getType()]);
}

/// Obtain the raw pointer to the underlying data of a MATLAB array `arr`
/// without invoking a deep copy. Due to C++ being statically typed and MATLAB
/// being dynamically typed, a specific combination of operations are necessary
/// to extract the raw pointer, possibly unsafely.
template <typename T>
const T *rtbf_base::getDataPtr(const matlab::data::Array &arr) {
  checkTypeMatch<T>(arr);  // First, ensure that data types match
  const matlab::data::TypedArray<T> arr_t = arr;
  matlab::data::TypedIterator<const T> it(arr_t.begin());
  return it.operator->();
}

/// Obtain the raw pointer to a data array inside of a MATLAB struct at a given
/// field, checking for existence and then invoking `getDataPtr()`.
template <typename T>
const T *rtbf_base::getReqArray(const matlab::data::StructArray &sarr,
                                std::string field, int sindex) {
  checkFieldExists(sarr, field, sindex);
  return getDataPtr<T>(sarr[sindex][field]);
}

/// Obtain the raw pointer to a data array inside of a MATLAB struct at a given
/// field if it exists, and return `nullptr` otherwise.
template <typename T>
const T *rtbf_base::getOptArray(const matlab::data::StructArray &sarr,
                                std::string field, int sindex) {
  try {
    getReqArray<T>(sarr, field, sindex);
  } catch (matlab::data::InvalidFieldNameException) {
    return nullptr;
  }
}

/// Obtain the raw pointer to a data array inside of a MATLAB struct at a given
/// field, checking for existence and checking that the array has dimensions
/// `dims`, and then invoking `getDataPtr()`.
template <typename T>
const T *rtbf_base::getReqArrayWithDims(const matlab::data::StructArray &sarr,
                                        std::string field,
                                        std::vector<size_t> dims, int sindex) {
  checkFieldExists(sarr, field, sindex);
  matlab::data::ArrayDimensions adims = sarr[sindex][field].getDimensions();
  if (adims.size() != dims.size() ||
      !std::equal(dims.begin(), dims.end(), adims)) {
    printErr("Requsted dimensions {} but field {} has dimensions {}.",
             vecString(dims), field, vecString(adims));
  }
  return getDataPtr<T>(sarr[sindex][field]);
}

/// Obtain the raw pointer to a data array inside of a MATLAB struct at a given
/// field if it exists and checking that the array has dimensions `dims`, and
/// return `nullptr` otherwise.
template <typename T>
const T *rtbf_base::getOptArrayWithDims(const matlab::data::StructArray &sarr,
                                        std::string field,
                                        std::vector<size_t> dims, int sindex) {
  try {
    return getReqArrayWithDims<T>(sarr, field, dims, sindex);
  } catch (matlab::data::InvalidFieldNameException) {
    return nullptr;
  }
}

/// Get a TensorView of a given MATLAB data array
template <typename T>
TensorView<T> rtbf_base::getTensor(const matlab::data::Array &arr,
                                   std::string name) {
  std::vector<size_t> dims = arr.getDimensions();
  return TensorView<T>(dims, const_cast<T *>(getDataPtr<T>(arr)), -1, name);
}
template <>
TensorView<cuda::std::complex<short>> rtbf_base::getTensor(
    const matlab::data::Array &arr, std::string name) {
  std::vector<size_t> dims = arr.getDimensions();
  return TensorView<cuda::std::complex<short>>(
      dims,
      reinterpret_cast<cuda::std::complex<short> *>(
          const_cast<std::complex<short> *>(
              getDataPtr<std::complex<short>>(arr))),
      -1, name);
}
template <>
TensorView<cuda::std::complex<float>> rtbf_base::getTensor(
    const matlab::data::Array &arr, std::string name) {
  std::vector<size_t> dims = arr.getDimensions();
  return TensorView<cuda::std::complex<float>>(
      dims,
      reinterpret_cast<cuda::std::complex<float> *>(
          const_cast<std::complex<float> *>(
              getDataPtr<std::complex<float>>(arr))),
      -1, name);
}

/// Obtain the raw pointer to a data array inside of a MATLAB struct at a given
/// field, checking for existence and checking that the array has dimensions
/// `dims`, and then invoking `getDataPtr()`.
template <typename T>
TensorView<T> rtbf_base::getReqTensor(const matlab::data::StructArray &sarr,
                                      std::string field, int sindex) {
  checkFieldExists(sarr, field, sindex);
  matlab::data::ArrayDimensions adims = sarr[sindex][field].getDimensions();
  return getTensor<T>(sarr[sindex][field], field);
}

/// Obtain the raw pointer to a data array inside of a MATLAB struct at a given
/// field if it exists and checking that the array has dimensions `dims`, and
/// return `nullptr` otherwise.
template <typename T>
std::optional<TensorView<T>> rtbf_base::getOptTensor(
    const matlab::data::StructArray &sarr, std::string field, int sindex) {
  try {
    // Return a Tensor<T> view of the data, wrapped in a unique_ptr
    return getReqTensor<T>(sarr, field, sindex);
  } catch (matlab::data::InvalidFieldNameException) {
    return std::nullopt;
  }
}

/// Obtain the raw pointer to a data array inside of a MATLAB struct at a given
/// field, checking for existence and checking that the array has dimensions
/// `dims`, and then invoking `getDataPtr()`.
template <typename T>
TensorView<T> rtbf_base::getReqTensorWithDims(
    const matlab::data::StructArray &sarr, std::string field,
    std::vector<size_t> dims, int sindex) {
  checkFieldExists(sarr, field, sindex);
  matlab::data::ArrayDimensions adims = sarr[sindex][field].getDimensions();
  if ((dims.size() == 1 && prod(adims) != prod(dims)) ||
      (dims.size() > 1 && adims != dims)) {
    printErr("Requsted dimensions {} but field {} has dimensions {}.",
             vecString(dims), field, vecString(adims));
  }
  // Return a Tensor<T> view of the data, wrapped in a unique_ptr
  return getReqTensor<T>(sarr, field, sindex);
}

/// Obtain the raw pointer to a data array inside of a MATLAB struct at a given
/// field if it exists and checking that the array has dimensions `dims`, and
/// return `nullptr` otherwise.
template <typename T>
std::optional<TensorView<T>> rtbf_base::getOptTensorWithDims(
    const matlab::data::StructArray &sarr, std::string field,
    std::vector<size_t> dims, int sindex) {
  try {
    // Return a Tensor<T> view of the data, wrapped in a unique_ptr
    return getReqTensorWithDims<T>(sarr, field, dims, sindex);
  } catch (matlab::data::InvalidFieldNameException) {
    return std::nullopt;
  }
}

/// Get a required C++ string from a MATLAB struct, or throw a MATLAB error
/// otherwise. The string can either be a MATLAB char array (single-quotes) or a
/// MATLAB string array (double-quotes).
std::string rtbf_base::getReqString(const matlab::data::StructArray &sarr,
                                    std::string field, int sindex) {
  checkFieldExists(sarr, field, sindex);
  // checkTypeMatch<matlab::data::MATLABString>(sarr[sindex][field]);
  if (sarr[sindex][field].getType() == matlab::data::ArrayType::CHAR) {
    matlab::data::CharArray str = sarr[sindex][field];
    return str.toAscii();
    // } else if (sarr[sindex][field].getType() ==
    // matlab::data::ArrayType::MATLAB_STRING) {
    //   matlab::data::TypedArray<matlab::data::MATLABString> str =
    //   sarr[sindex][field]; return str[0];
  } else {
    printErr("{} is not a char array or a string.", field);
    return "";
  }
}

/// Get an optional C++ string from a MATLAB struct, or return a default string
/// otherwise. The string can either be a MATLAB char array (single-quotes) or a
/// MATLAB string array (double-quotes).
std::string rtbf_base::getOptString(const matlab::data::StructArray &sarr,
                                    std::string field,
                                    std::string defaultString, int sindex) {
  try {
    checkFieldExists(sarr, field, sindex);
    return getReqString(sarr, field, sindex);
  } catch (matlab::data::InvalidFieldNameException) {
    return defaultString;
  }
}

int rtbf_base::getFirstAvailableGPU() {
  int nDevices;
  CCE(cudaGetDeviceCount(&nDevices));
  for (int i = 0; i < nDevices; i++) {
    cudaDeviceProp prop;
    CCE(cudaGetDeviceProperties(&prop, i));
    if (prop.major >= 3) return i;
  }
  printErr("No compatible GPUs found (Compute Capability >= 3.0)");
  return -1;
}

template <typename T>
auto rtbf_base::copyToMATLABArray(const std::shared_ptr<Tensor<T>> &D) {
  // Handle cuda::std::complex differently (cast to std::complex)
  if constexpr (is_complex<T>() && is_cuda_complex<T>()) {
    typedef std::complex<decltype(abs(T(0)))> T1;
    auto result = factory.createArray<T1>(D->getDimensions());
    auto rptr = const_cast<T1 *>(getDataPtr<T1>(result));
    D->copyTo(reinterpret_cast<T *>(rptr));
    return result;
  } else {
    matlab::data::TypedArray<T> result =
        factory.createArray<T>(D->getDimensions());
    auto rptr = const_cast<T *>(getDataPtr<T>(result));
    D->copyTo(rptr);
    return result;
  }
  unreachable();
}

template <typename T>
auto rtbf_base::copyToMATLABArrayAsync(const std::shared_ptr<Tensor<T>> &D,
                                       cudaStream_t stream) {
  // Handle cuda::std::complex differently (cast to std::complex)
  if constexpr (is_complex<T>() && is_cuda_complex<T>()) {
    typedef std::complex<decltype(abs(T(0)))> T1;
    auto result = factory.createArray<T1>(D->getDimensions());
    auto rptr = const_cast<T1 *>(getDataPtr<T1>(result));
    D->copyToAsync(reinterpret_cast<T *>(rptr), stream);
    return result;
  } else {
    matlab::data::TypedArray<T> result =
        factory.createArray<T>(D->getDimensions());
    auto rptr = const_cast<T *>(getDataPtr<T>(result));
    D->copyToAsync(rptr, stream);
    return result;
  }
}

template <typename T1, typename T2>
auto rtbf_base::copyToMATLABArray(Operator<Tensor<T1>, Tensor<T2>> &Op) {
  copyToMATLABArray(Op.getOutput());
}

template <typename T1, typename T2>
auto rtbf_base::copyToMATLABArrayAsync(Operator<Tensor<T1>, Tensor<T2>> &Op,
                                       cudaStream_t stream) {
  copyToMATLABArrayAsync(Op.getOutput(), stream);
}

}  // namespace rtbf
#endif